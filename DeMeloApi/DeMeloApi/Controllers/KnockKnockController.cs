﻿using System;
using System.Net;
using System.Web.Http;

namespace DeMeloApi.Controllers
{
    [RoutePrefix("api/")]
    public class KnockKnockController : ApiController
    {

        // GET api/token
        [HttpGet]
        public string Token()
        {
            return "db9479eb-a872-479f-b33e-23b176382c2a";
        }

        // GET api/Fibbonacci?n=5
        [HttpGet]
        public long Fibonacci(long n)
        {
            try
            {
                return Fib(n);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        // GET api/ReverseWords?sentence=value
        [HttpGet]
        public string ReverseWords(string sentence)
        {
            if (string.IsNullOrWhiteSpace(sentence))
            {
                return string.Empty;
            }

            char[] arValue = sentence.ToCharArray();
            Array.Reverse(arValue);
            return (new string(arValue));
        }

        // Needed this overload to respond with an empty string if no value has been submitted.
        // GET api/ReverseWords
        [HttpGet]
        public string ReverseWords()
        {
            return string.Empty;
        }

        // GET api/values/5
        [HttpGet]
        public string TriangleType(int a, int b, int c)
        {
            return GetTriangleType(a, b, c);
        }

        #region Auxiliay Functions

        /// <summary>
        /// Eveluates the type of triangle and returns.
        /// </summary>
        private string GetTriangleType(int a, int b, int c)
        {
            // No sides, no joy...
            if (a < 1 || b < 1 || c < 1)
            {
                return "Error";
            }
            // checking if the triangle is valid or not
            if (!((a + b > c) && (a + c > b) && (b + c > a)))
            {
                return "Error";
            }

            // at this stage we know it's valid, let's determine the type

            if (a == b && b == c)
            {
                return "Equilateral";
            }
            else if (a != b && b != c && c != a)
            {
                return "Scalene";
            }

            return "Isosceles";
        }

        /// <summary>
        /// Returns te nth number in the fibonnaci sequence
        /// </summary>
        /// <param name="nth">The index (n) of the Fibonacci sequence</param>
        /// <returns></returns>
        private long Fib(long nth)
        {
            if (nth > 92 || nth < -92)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (nth == -1 || nth == 0 || nth == 1)
            {
                return nth;
            }

            long fn = nth;

            // "negafibonacci" numbers yelds the same "positive" sequence multipiled by -1
            if (nth < 0)
            {
                fn *= -1; // make it positive                
            }

            long a = 0;
            long b = 1;
            long next = a + b;

            for (long i = 1; i < fn; i++)
            {
                next = a + b;
                a = b;
                b = next;
            }

            if (nth < 0)
            {
                next *= -1; // now we "undo" and convert the operation to positive           
            }

            return next;
        }
        #endregion

    }
}
