﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DeMeloApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));

            /*KnockKnock section*/
            config.Routes.MapHttpRoute(
                name: "Token",
                routeTemplate: "api/Token",
                defaults: new { controller = "KnockKnock", action = "Token" }
            );
            config.Routes.MapHttpRoute(
                name: "Fibbonacci",
                routeTemplate: "api/Fibonacci",
                defaults: new { controller = "KnockKnock", action = "Fibonacci" }
            );
            config.Routes.MapHttpRoute(
                name: "ReverseWords",
                routeTemplate: "api/ReverseWords",
                defaults: new { controller = "KnockKnock", action = "ReverseWords" }
            );
            config.Routes.MapHttpRoute(
                name: "TriangleType",
                routeTemplate: "api/TriangleType",
                defaults: new { controller = "KnockKnock", action = "TriangleType" }
            );
            ///End of Knock Knock Section

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
